var gulp = require('gulp');
var sass = require('gulp-sass');


/* SASS to CSS folder */
gulp.task('sass',function(){
	gulp.src('bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss')
	.pipe(sass())
		.pipe(gulp.dest('app/css'));
});

gulp.task('default',['sass']);